; sprites
load_sprites:
    ldx     #$00                ; first index
@loop:
    lda     sprites, x      ; read spritedata from memory
    sta     $0200, x            ; and write it to $0200 + x.
    inx                         ; x++

    cpx     #$30                ; check if $10 (16) sprites
    bne     @loop               ; has been loaded

    rts

;--------------------------
sprites:
pad_sprite:
    ;----   y    tile attr x
    .byte   $80, $00, $00, $10
    .byte   $88, $01, $00, $10
    .byte   $90, $01, $00, $10
    .byte   $98, $02, $00, $10

ball_sprite:
    ;----   y    tile attr x
    .byte   $88, $03, $00, $50

text_sprite:
    ;----   y    tile attr x
    .byte   $06, $16, $02, $18  ; 's'
    .byte   $06, $06, $02, $20  ; 'c'
    .byte   $06, $12, $02, $28  ; 'o'
    .byte   $06, $15, $02, $30  ; 'r'
    .byte   $06, $08, $02, $38  ; 'e'

    .byte   $06, $1d, $02, $48  ; '0' $0228
    .byte   $06, $1d, $02, $50  ; '0' $022c
