.segment "HEADER"
	.byte   "NES", $1A      ; iNES header identifier
	.byte   2               ; 2x 16KB PRG code
	.byte   1               ; 1x  8KB CHR data
	.byte   $01, $00        ; mapper 0, vertical mirroring

;;;;;;;;;;;;;;;

;;; "nes" linker config requires a STARTUP section, even if it's empty
.segment "STARTUP"


.segment "ZEROPAGE"

; variables
;---------------------
var_gamestate:  .res 1
var_score:      .res 1
var_buttons:	.res 1
var_pad_y:		.res 1
var_ball_x:		.res 1
var_ball_y:		.res 1
var_ball_vel:	.res 1
var_ball_flags:	.res 1 		; bit 0 = y, bit 1 = x
.segment "CODE"

;---------------------
; constants
con_title:		.byte $00
con_game:       .byte $01
con_fail:       .byte $02

con_top:        .byte $10
con_bottom:     .byte $e0
con_left:		.byte $18
con_right:		.byte $f8

con_pad_x:		.byte $10

con_sprites: 	.byte $30

con_key_right:	.byte %00000001
con_key_left:	.byte %00000010
con_key_down:	.byte %00000100
con_key_up:		.byte %00001000
con_key_start:	.byte %00010000
con_key_select:	.byte %00100000
con_key_b:		.byte %01000000
con_key_a: 		.byte %10000000

con_zero: 		.byte $1d
con_nine: 		.byte $26
;---------------------

reset:
	sei                     ; disable IRQs
	cld                     ; disable decimal mode
	ldx     #$40
	stx     $4017           ; dsiable APU frame IRQ
	ldx     #$ff            ; Set up stack
	txs                     ;  .
	inx                     ; now X = 0
	stx     $2000           ; disable NMI
	stx     $2001           ; disable rendering
	stx     $4010           ; disable DMC IRQs

	jsr 	vblank_wait
	;jsr 	clear_memory
	jsr 	vblank_wait

_main:
	jsr		setup_palette
	jsr		load_sprites

	; init pad
	lda  	#$a0
	sta 	var_pad_y

	; init ball
	lda 	#$80
	sta 	var_ball_y
	sta 	var_ball_x

	lda 	#%00010001
	sta 	var_ball_vel

	lda 	#%00000000
	sta 	var_ball_flags

; display everything
    lda     #%10010000
    sta     $2000

    lda     #%00011110
    sta     $2001

forever:
	jmp     forever

nmi:
	lda     #$00            ; lowbyte
	sta     $2003

	lda     #$02            ; high byte
	sta     $4014

	jsr 	read_controller
	jsr 	move_pad

	; update pad position
	lda 	var_pad_y
	sta 	$0200			; 1st block
	adc 	#$08
	sta 	$0204			; 2st block
	adc 	#$08
	sta 	$0208			; 3st block
	adc 	#$08
	sta 	$020c			; 4st block

	jsr 	update_ball
	;jsr 	update_score

	rti                     ;return

move_pad:
	lda 	var_buttons
	and 	con_key_up
	bne 	@move_up

	lda 	var_buttons
	and 	con_key_down
	bne 	@move_down

	jmp @done
@move_up:
	; check for top border
	lda 	var_pad_y
	cmp 	con_top
	bcc 	@done

	dec 	var_pad_y
	dec 	var_pad_y
	jmp @done

@move_down:
	; check for bottom border
	lda 	var_pad_y
	adc 	#$20
	cmp 	con_bottom
	bcs 	@done
	inc 	var_pad_y
	inc 	var_pad_y
	jmp @done

@done:
	rts

update_ball:
	lda 	var_ball_vel
	lsr 	a 				; shift left
	lsr 	a 				; to reach
	lsr 	a 				; x velocity (11110000)
	lsr 	a 				; of the byte
	tax

; ------- update x position
	lda 	var_ball_flags
	and 	#%00000010
	lsr 	a

@x_loop:
	cmp 	#$00
	bne 	@dec_x

	; increment x
	inc 	var_ball_x
	jmp 	@x_next
@dec_x:
	; decrement x
	dec 	var_ball_x
@x_next:
	dex
	bne 	@x_loop

; ------- update y position
	lda 	var_ball_vel

	and 	#%00001111
	tax

	lda 	var_ball_flags
	and 	#%00000001

@y_loop:
	cmp 	#$00
	bne 	@dec_y

	inc 	var_ball_y
	jmp 	@next_y
@dec_y:
	dec 	var_ball_y
@next_y:
	dex
	bne 	@y_loop

; ------- update direction flags
	lda 	var_ball_x
	cmp 	con_left
	bcs 	@left_ok

; check if ball touches pad
	lda 	var_pad_y
	cmp 	var_ball_y
	bcs 	@fail 			; compare, if ball_y is less than pad_y

	adc 	#$20 			; add pad's height
	cmp 	var_ball_y
	bcc 	@fail 			; compare, if ball_y is more than pad_y

	jsr 	update_score

	jmp 	@toggle_x

@fail:
	jsr 	fail

@left_ok:
	cmp 	con_right
	bcs 	@toggle_x

	jmp 	@over_x

@toggle_x:
	lda 	var_ball_flags
	eor 	#%00000010
	sta 	var_ball_flags

@over_x:
	lda 	var_ball_y
	cmp 	con_top
	bcc 	@toggle_y

	cmp 	con_bottom
	bcs 	@toggle_y

	jmp 	@over_y

@toggle_y:
	lda 	var_ball_flags
	eor 	#%00000001
	sta 	var_ball_flags

@over_y:

; update ball position
	lda 	var_ball_y
	sta 	$0210
	lda 	var_ball_x
	sta 	$0213

	rts

fail:
	lda 	#$80
	sta 	var_ball_x
	sta 	var_ball_y
	lda 	#$00
	sta 	var_score

	jsr clear_score

	rts

update_score:
; increment ones
	inc 	$022d

	lda 	$022d
	cmp 	con_nine
	bcc 	@ok

	lda 	con_zero
	sta 	$022d

; increment tens
	inc 	$0229

	lda 	$0229
	cmp 	con_nine
	bcc 	@ok

	lda 	con_zero
	sta 	$0229

@ok:
	rts

clear_score:
	lda 	con_zero
	sta 	$022d
	sta 	$0229
	rts

.include	"utils.inc"
.include 	"sprites.inc"

;;;;;;;;;;;;;;

.segment "VECTORS"

		.word   0, 0, 0         ; Unused, but needed to advance PC to $fffa.
		;; When an NMI happens (once per frame if enabled) the label nmi:
		.word   nmi
		;; When the processor first turns on or is reset, it will jump to the
		;; label reset:
		.word   reset
		;; External interrupt IRQ is not used in this tutorial
		.word   0

;;;;;;;;;;;;;;

.segment "CHARS"
		.incbin "pong.chr"
