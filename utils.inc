;--------------------------
vblank_wait:
    bit     $2002
    bpl     vblank_wait
    rts

;--------------------------
clear_memory:
    lda     #$00
    sta     $0000, x
    sta     $0100, x
    sta     $0200, x
    sta     $0300, x
    sta     $0400, x
    sta     $0500, x
    sta     $0600, x
    sta     $0700, x

    lda     #$fe
    sta     $0200, x

    inx
    bne     clear_memory
    rts

;--------------------------
setup_palette:
clear_palette:  
    ;; Need clear both palettes to $00. Needed for Nestopia. Not
    ;; needed for FCEU* as they're already $00 on powerup.
    lda     $2002           ; Read PPU status to reset PPU address
    lda     #$3f            ; Set PPU address to BG palette RAM ($3F00)
    sta     $2006
    lda     #$00
    sta     $2006

    ldx     #$20            ; Loop $20 times (up to $3F20)
    lda     #$00            ; Set each entry to $00
@loop:
    sta     $2007
    dex
    bne     @loop

load_palette:
    ldx     #$00            ; set X to 0. X is loop index now
@loop:
    lda     paletteData, x  ; load data from andress of
                            ; paletteData + x
    sta     $2007           ; write it to PPU
    inx                     ; x++
    cpx     #$20            ; compare to hex 20 (dec 32)
    bne     @loop
    rts

paletteData:
    ;; Background palette
    .byte   $0F,$31,$32,$33
    .byte   $0F,$35,$36,$37
    .byte   $0F,$39,$3A,$3B
    .byte   $0F,$3D,$3E,$0F
    ;; Sprite palette
    .byte   $0F,28,$01,$3a
    .byte   $0F,$02,$38,$3C
    .byte   $0F,$1C,$15,$14
    .byte   $0F,$02,$38,$3C

;--------------------------
read_controller:
	LDA #$01
	STA $4016
	LDA #$00
	STA $4016
	LDX #$08
@loop:
	LDA $4016
	LSR A          		; bit0 -> Carry
	ROL var_buttons	  	; bit0 <- Carry
	DEX
	BNE @loop
	RTS